const express = require("express");
const router = express.Router();

const productController = require("./../controllers/productController")

const auth = require("./../auth");

//Get All Products
router.get("/all", (req, res) =>{
	productController.getAll()
	.then(result => res.send(result));
})

//Get All Active Products
router.get("/active", (req, res) => {
	productController.getAllActive()
	.then(result => res.send(result));
})




//get a specific product using findById()
router.get("/:productId", auth.verify, (req, res) => {
	productController.getProduct(req.params.productId)
	.then(resultFromController => res.send(resultFromController))
})


module.exports = router;
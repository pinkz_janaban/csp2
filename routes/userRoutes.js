const express = require('express');
//Create routes

//Router() handles the requests
const router = express.Router();

//User model
//const user = require('./../models/user');

//User Controller
const userController = require('./../controllers/userController');

const auth = require('./../auth');

//checking of email exist
router.post('/email-exists', (req, res) => {
	userController.checkEmail(req.body.email).then( result => res.send(result))

});

//register a user
router.post("/register", (req, res)=>{
	userController.register(req.body)
	.then(result => res.send(result));
})

//login user
router.post("/login", (req, res) =>{
	userController.login(req.body)
	.then(result => res.send(result));
})

//Add Product
router.post("/", auth.verify, (req, res) => {
	let data = {
		
		userId: auth.decode(req.headers.authorization).id,
		
		productId: req.body.productId
	}
	userController.orders(data).then(result => res.send(result))
})


module.exports = router;
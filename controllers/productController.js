const user = require('./../models/user');

const product = require('./../models/product');

const auth = require('./../auth');

const bcrypt = require('bcrypt');

//Get All Products
module.exports.getAllproduct = () => {
	
	return product.find().then( (result, error) => {
		if(error){
			return false
		}else{
			return result
		}
	})
}

//Get All Active Products
module.exports.getActiveproduct = () => {
	return product.find({isActive: true}).then( (result, error) => {
		if(err){
			return false
		} else {
			return result
		}
	})
}
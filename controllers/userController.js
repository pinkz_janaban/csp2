const user = require('./../models/user');

const product = require("./../models/product")

const auth = require('./../auth');

const bcrypt = require('bcrypt');


module.exports.checkEmail = (email) => {
	return user.findOne({email: email}).then( (result, error) => {
		console.log(result)	//null

		if(result !== null){
			return (false)
		} else {

			if(result === null){
				//send back the response to the client
				return (true)
			} else {
				return (error)
			}
		}
	})
	
}

module.exports.register = (reqBody) => {
	const {email, password} = reqBody

	const newuser = new user({
		email: email,
		password: bcrypt.hashSync(password, 10),
		
	})

	//save the newUser object to  the database
	return newuser.save().then( (result, error) => {
		if(result){
			return true
		} else {
			return error
		}
	})
}

module.exports.login = (reqBody) => {
	const {email, password} = reqBody

	return user.findOne({email: email}).then( (result, error) => {
		// console.log(result)

		if(result == null){
			console.log('email null');
			return false
		} else {

			let isPwCorrect = bcrypt.compareSync(password, result.password)	

			if(isPwCorrect == true){
			//return json web token
				//invoke the function which creates the token upon logging in
				// requirements in creating a token:
					// if password matches from existing pw from db
					return {access: auth.createAccessToken(result)}
					// return auth.createAccessToken(result)
			} else {
				return false
			}
		}
	})
	
}

//Add Products
module.exports.Add = async (data) => {
	const {userId, productId} = data


	//look for matching document of a user
	const userAdd = await User.findById(userId).then( (result, err) => {
		if(err){
			return err
		} else {
			// console.log(result)
			result.orders.push({productId: productId})

			return result.save().then( result => {
				return true
			})
		}

	})

	//look for matching document of a user
	const productAdd = await product.findById(productId).then( (result, err) => {
		if(err){
			return err
		} else {

			result.ordered.push({UserId: UserId})
			//console.log(result)
			return result.save().then( result => {
				return true
			})
		}
	})

	//to return only one value for the function enroll

	if(userAdd && productAdd){
		return true
	} else {
		return false
	}
}